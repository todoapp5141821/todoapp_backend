FROM maven:3.8-openjdk-18 as builder

WORKDIR /app

COPY pom.xml /app
COPY src /app/src


RUN mvn clean package -DskipTests

FROM openjdk:18-jdk-alpine
RUN apk --update add postgresql14-client
COPY --from=builder /app/target/todoapp-0.0.1-SNAPSHOT.jar /app/todoapp-0.0.1-SNAPSHOT.jar

EXPOSE 8080

CMD ["java", "-jar", "/app/todoapp-0.0.1-SNAPSHOT.jar"]