package com.todoapp.todoapp.repository;

import com.todoapp.todoapp.model.Todo;
import com.todoapp.todoapp.model.TodoResponse;
import com.todoapp.todoapp.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TodoRepository extends JpaRepository<Todo, Long> {

    List<Todo> findAllByUser(User user);
}
