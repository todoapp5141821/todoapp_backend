package com.todoapp.todoapp;

import com.todoapp.todoapp.configuration.RsaKeyProperties;
import com.todoapp.todoapp.model.User;
import com.todoapp.todoapp.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableConfigurationProperties(RsaKeyProperties.class)
@SpringBootApplication
public class TodoappApplication {


	public static void main(String[] args) {
		SpringApplication.run(TodoappApplication.class, args);
	}

//	@Bean
//	CommandLineRunner commandLineRunner(UserRepository repository, PasswordEncoder encoder) {
//		return args -> {
//			repository.save(new User("edek", "kredek", "kredek@g.com", encoder.encode("pass"), "USER_ADMIN"));
//		};
//	}

}
