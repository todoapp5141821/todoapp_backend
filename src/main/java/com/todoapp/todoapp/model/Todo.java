package com.todoapp.todoapp.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;


@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Todo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User user;
    @Size(max = 250)
    private String title;
    private String status;
    private LocalDate deadline;
    private LocalDate createdOn;
    private LocalDate updatedOn;

    public Todo(String title, LocalDate deadline) {
        this.title = title;
        this.status = "todo";
        this.deadline = deadline;
        this.createdOn = LocalDate.now();
    }
}
