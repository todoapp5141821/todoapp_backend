package com.todoapp.todoapp.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;


@Entity
@Setter
@Getter
@ToString
@NoArgsConstructor
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    private String username;
    private String email;
    private String password;
    private String role;

    public User(String email, String password, String role) {
        this.username = email;
        this.email = email;
        this.password = password;
        this.role = role;
    }
}
