package com.todoapp.todoapp.model;

import javax.validation.constraints.NotBlank;

public record LoginRequest(@NotBlank String email, String password) {
}
