package com.todoapp.todoapp.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;


@Getter
@Setter
@AllArgsConstructor
public class CreateUserDto {

    @Email(message = "Email is not valid", regexp = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$")
    @NotEmpty(message = "Email cannot be empty")
    private String email;
    @NotBlank(message = "Password is mandatory")
    @Pattern(regexp = ".{8,}",
            message = "Password minimum eight characters, at least one letter and one number")
    private String password;
    @NotBlank(message = "Confirm the password!")
    private String confirmPass;
}
