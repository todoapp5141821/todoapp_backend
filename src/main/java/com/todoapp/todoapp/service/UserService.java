package com.todoapp.todoapp.service;


import com.todoapp.todoapp.dto.CreateUserDto;
import com.todoapp.todoapp.dto.UpdateUserDto;
import com.todoapp.todoapp.model.User;

import java.util.List;

public interface UserService {

    User create(CreateUserDto dto);
    User read(Long id);
    List<User> readAll();
    User update(UpdateUserDto dto, Long id);
    void delete(Long id);

}
