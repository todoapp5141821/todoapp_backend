package com.todoapp.todoapp.service;

import com.todoapp.todoapp.dto.CreateTodoDto;
import com.todoapp.todoapp.dto.UpdateTodoDto;
import com.todoapp.todoapp.model.Todo;
import com.todoapp.todoapp.model.CreateTodoResponse;
import com.todoapp.todoapp.model.TodoResponse;
import com.todoapp.todoapp.model.User;
import com.todoapp.todoapp.repository.TodoRepository;
import com.todoapp.todoapp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.rmi.NoSuchObjectException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TodoServiceImpl implements TodoService{

    private final TodoRepository todoRepository;
    private final UserRepository userRepository;

    @Autowired
    public TodoServiceImpl(TodoRepository todoRepository, UserRepository userRepository) {
        this.todoRepository = todoRepository;
        this.userRepository = userRepository;
    }

    @Override
    public CreateTodoResponse create(CreateTodoDto createTodoDto, Authentication authentication) throws NoSuchObjectException {
        Todo todo = new Todo();
        Optional<User> optionalUser = userRepository.findByUsername(authentication.getName());

        if(optionalUser.isEmpty()) {
            throw new NoSuchObjectException("No such user");
        }
        User user = optionalUser.get();
        todo.setStatus("TODO");
        todo.setCreatedOn(LocalDate.now());
        todo.setUser(user);
        todo.setTitle(createTodoDto.getTitle());
        todo.setDeadline(createTodoDto.getDeadline());

        todoRepository.save(todo);

        return new CreateTodoResponse(todo.getId(), "Todo created");
    }

    @Override
    public TodoResponse read(Long id) throws NoSuchObjectException {
        Optional<Todo> optionalTodo =  todoRepository.findById(id);
        if(optionalTodo.isEmpty()) {
            throw new NoSuchObjectException("No such todo");
        }
        Todo todo = optionalTodo.get();
        TodoResponse todoResponse = new TodoResponse();
        todoResponse.setId(todo.getId());
        todoResponse.setDeadline(todo.getDeadline());
        todoResponse.setTitle(todo.getTitle());
        return todoResponse;
    }

    @Override
    public List<TodoResponse> readAll(Authentication authentication) throws NoSuchObjectException {
        Optional <User> optionalUser = userRepository.findByUsername(authentication.getName());
        List<TodoResponse> todosResponse = new ArrayList<>();
        if(optionalUser.isEmpty()) {
            throw new NoSuchObjectException("No such user");
        }
        User user = optionalUser.get();
        List<Todo> todos = todoRepository.findAllByUser(user);
        for (Todo todo : todos) {
            TodoResponse todoResponse = new TodoResponse();
            todoResponse.setId(todo.getId());
            todoResponse.setTitle(todo.getTitle());
            todoResponse.setDeadline(todo.getDeadline());
            todosResponse.add(todoResponse);
        }
        return todosResponse;
    }

    @Override
    public TodoResponse update(UpdateTodoDto updateTodoDto, Long id) throws NoSuchObjectException {
        Optional<Todo> optionalTodo = todoRepository.findById(id);

        if(optionalTodo.isEmpty()) {
            throw new NoSuchObjectException("No such todo");
        }

        Todo todo = optionalTodo.get();

        todo.setUpdatedOn(LocalDate.now());
        todo.setStatus(updateTodoDto.getStatus());
        todo.setTitle(updateTodoDto.getTitle());
        todo.setDeadline(updateTodoDto.getDeadline());

        todoRepository.save(todo);

        TodoResponse todoResponse = new TodoResponse();
        todoResponse.setTitle(todo.getTitle());
        todoResponse.setDeadline(todo.getDeadline());
        todoResponse.setId(todo.getId());

        return todoResponse;
    }

    @Override
    public void delete(Long id) {
        todoRepository.deleteById(id);
    }
}
