package com.todoapp.todoapp.service;

import com.todoapp.todoapp.dto.CreateTodoDto;
import com.todoapp.todoapp.dto.UpdateTodoDto;
import com.todoapp.todoapp.model.Todo;
import com.todoapp.todoapp.model.CreateTodoResponse;
import com.todoapp.todoapp.model.TodoResponse;
import org.springframework.security.core.Authentication;

import java.rmi.NoSuchObjectException;
import java.util.List;


public interface TodoService {

    CreateTodoResponse create(CreateTodoDto createTodoDto, Authentication authentication) throws NoSuchObjectException;

    TodoResponse read(Long id) throws NoSuchObjectException;

    List<TodoResponse> readAll(Authentication authentication) throws NoSuchObjectException;

    TodoResponse update(UpdateTodoDto updateTodoDto, Long id) throws NoSuchObjectException;

    void delete(Long id);



}
