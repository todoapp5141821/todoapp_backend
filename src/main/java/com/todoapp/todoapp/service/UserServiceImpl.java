package com.todoapp.todoapp.service;



import com.todoapp.todoapp.dto.CreateUserDto;
import com.todoapp.todoapp.dto.UpdateUserDto;
import com.todoapp.todoapp.exception.ObjectAlreadyExistsException;
import com.todoapp.todoapp.model.User;
import com.todoapp.todoapp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    public UserServiceImpl(UserRepository userRepository, PasswordEncoder encoder) {
        this.userRepository = userRepository;
        this.encoder = encoder;
    }

    private final UserRepository userRepository;
    private final PasswordEncoder encoder;




    @Override
    public User create(CreateUserDto dto) {
        User user = new User();
        isEmailAvailable(dto.getEmail());
        doPassesMatch(dto.getPassword(), dto.getConfirmPass());
        user.setRole("USER");
        user.setUsername(dto.getEmail());
        user.setEmail(dto.getEmail());
        user.setPassword(encoder.encode(dto.getPassword()));

        return userRepository.save(user);
    }

    @Override
    public User read(Long id) {
        return null;
    }

    @Override
    public List<User> readAll() {
        return null;
    }

    @Override
    public User update(UpdateUserDto dto, Long id) {
        return null;
    }

    @Override
    public void delete(Long id) {

    }

    private boolean isEmailAvailable(String email) {
        if (userRepository.findByEmail(email).isEmpty()) {
            return true;
        }
        throw new ObjectAlreadyExistsException("The email is already taken");
    }

    private boolean doPassesMatch(String password, String confirm) {
        if (confirm.equals(password)) {
            return true;
        }
        throw new IllegalArgumentException("Passwords don't match");
    }
}
