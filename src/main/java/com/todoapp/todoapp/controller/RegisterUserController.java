package com.todoapp.todoapp.controller;


import com.todoapp.todoapp.dto.CreateUserDto;
import com.todoapp.todoapp.exception.ObjectAlreadyExistsException;
import com.todoapp.todoapp.model.ErrorResponse;
import com.todoapp.todoapp.model.RegisterResponse;
import com.todoapp.todoapp.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping("/api")
public class RegisterUserController {

    private final UserServiceImpl userService;

    @Autowired
    public RegisterUserController(UserServiceImpl userService) {
        this.userService = userService;
    }

    @PostMapping("/register")
    public RegisterResponse register(@Valid @RequestBody CreateUserDto createUserDto) {

       userService.create(createUserDto);

       return new RegisterResponse("User registered", createUserDto.getEmail());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ObjectAlreadyExistsException.class)
    public ErrorResponse handleValidationExceptions2(
            ObjectAlreadyExistsException ex) {
        return new ErrorResponse(ex.getMessage());
    }

}
