package com.todoapp.todoapp.controller;

import com.todoapp.todoapp.model.TestCors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/api/test-cors")
public class TestCorsController {


    @GetMapping
    public TestCors testCors() {
        LocalDateTime dateTime = LocalDateTime.now();
        return new TestCors( dateTime + " cors ok");
    }
}
