package com.todoapp.todoapp.controller;

import com.todoapp.todoapp.dto.CreateTodoDto;
import com.todoapp.todoapp.dto.UpdateTodoDto;
import com.todoapp.todoapp.model.Todo;
import com.todoapp.todoapp.model.CreateTodoResponse;
import com.todoapp.todoapp.model.TodoResponse;
import com.todoapp.todoapp.service.TodoServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.rmi.NoSuchObjectException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/api/todo")
public class TodoController {

    private final TodoServiceImpl todoService;

    @Autowired
    public TodoController(TodoServiceImpl todoService) {
        this.todoService = todoService;
    }

    @PostMapping("")
    public CreateTodoResponse add(@Valid @RequestBody CreateTodoDto dto, Authentication authentication) throws Exception {
        return todoService.create(dto, authentication);
    }

    @GetMapping("/{id}")
    public TodoResponse findById(@PathVariable("id") Todo todo) throws NoSuchObjectException {
        return todoService.read(todo.getId());
    }

    @GetMapping("")
    public List<TodoResponse> findAll(Authentication authentication) throws NoSuchObjectException {
        return todoService.readAll(authentication);
    }

    @PatchMapping("/{id}")
    public TodoResponse updateById(@Valid @RequestBody UpdateTodoDto updateTodoDto, @PathVariable Long id) throws NoSuchObjectException {
        return todoService.update(updateTodoDto, id);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id) {
        todoService.delete(id);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(Exception.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
