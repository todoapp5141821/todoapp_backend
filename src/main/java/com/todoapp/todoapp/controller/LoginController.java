package com.todoapp.todoapp.controller;

import com.todoapp.todoapp.model.ErrorResponse;
import com.todoapp.todoapp.model.LoginRequest;
import com.todoapp.todoapp.model.Response;
import com.todoapp.todoapp.model.User;
import com.todoapp.todoapp.repository.UserRepository;
import com.todoapp.todoapp.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api")
public class LoginController {

    private final TokenService tokenService;
    private final AuthenticationManager authenticationManager;
    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public LoginController(TokenService tokenService, AuthenticationManager authenticationManager, UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.tokenService = tokenService;
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping("/login")
    public Response login(@RequestBody LoginRequest userLogin) {
        Optional<User> user = userRepository.findByEmail(userLogin.email());
        if(user.isEmpty()) {
            throw new IllegalArgumentException("User not found");
        }
        if(!(passwordEncoder.matches(userLogin.password(), user.get().getPassword()))) {
            throw new IllegalArgumentException("Wrong login or password");
        }
        Authentication authentication = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(userLogin.email(), userLogin.password()));

        return new Response(tokenService.generateToken(authentication), userLogin.email());

    }


    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(IllegalArgumentException.class)
    public ErrorResponse handleValidationExceptions2(
            IllegalArgumentException ex) {
        return new ErrorResponse(ex.getMessage());
    }


}
