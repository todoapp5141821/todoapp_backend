package com.todoapp.todoapp.controller;



import com.todoapp.todoapp.model.Hello;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class HelloController {


    @GetMapping("/hello-world")
    public Hello helloWorld() {

        return new Hello("Hello World");
    }
}
