package com.todoapp.todoapp.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.todoapp.todoapp.model.Response;
import com.todoapp.todoapp.model.User;
import com.todoapp.todoapp.repository.UserRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
class LoginControllerTest {

    @Autowired
    private WebApplicationContext context;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder encoder;
    @Autowired
    ObjectMapper objectMapper;

    private User user;

    private MockMvc mvc;


    @BeforeEach
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

        user = userRepository.save(new User("kredek@gmail.com", encoder.encode("11111111"), "USER_ADMIN"));
    }

    @AfterEach
    public void clean() {
        userRepository.delete(user);
    }

    private final String CREDENTIALS = """
                                          {
                                             "email": "kredek@gmail.com",
                                             "password": "11111111" 
                                          }
                                          """;
    private final String BAD_CREDENTIALS = """
                                          {
                                             "email": "kredek@gmail.com",
                                             "password": "22222222" 
                                          }
                                          """;


    @Test
    void shouldReturnStatusOk() throws Exception {
        MvcResult result = this.mvc.perform(post("/api/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(CREDENTIALS))
                .andExpect(status().isOk())
                .andReturn();

    }


    @Test
    void shouldReturnUnautorized() throws Exception {
        MvcResult result = this.mvc.perform(post("/api/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(BAD_CREDENTIALS))
                .andExpect(status().isBadRequest())
                .andReturn();


    }

    @Test
    void shouldReturn200() throws Exception {
        MvcResult result = this.mvc.perform(post("/api/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(CREDENTIALS))
                .andExpect(status().isOk())
                .andReturn();
        String token = result.getResponse().getContentAsString();
        Response response = objectMapper.readValue(token, Response.class);
        this.mvc.perform(get("/api/hello-world")
                        .header("Authorization", "Bearer " + response.getToken()))
                .andExpect(status().isOk());

    }

    @Test
    void shouldReturn401() throws Exception {

        this.mvc.perform(get("/api/hello-world")
                        .header("Authorization", "Bearer " + "fakeToken"))
                .andExpect(status().isUnauthorized());

    }


}