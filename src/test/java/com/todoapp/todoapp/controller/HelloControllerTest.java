package com.todoapp.todoapp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.todoapp.todoapp.model.Response;
import com.todoapp.todoapp.model.User;
import com.todoapp.todoapp.repository.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;


@SpringBootTest
class HelloControllerTest {


    @Autowired
    private WebApplicationContext context;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder encoder;
    @Autowired
    private ObjectMapper objectMapper;

    private User user;

    private MockMvc mvc;


    @BeforeEach
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

        user = userRepository.save(new User( "kredek@g.com",
                encoder.encode("11111111"), "USER_ADMIN"));
    }


    @AfterEach
    public void clean() {
        userRepository.delete(user);
    }



    private final String CREDENTIALS = """
                                          {
                                             "email": "kredek@g.com",
                                             "password": "11111111" 
                                          }
                                          """;

    @Test
    void rootWhenUnauthenticatedThen401() throws Exception {
        this.mvc.perform(get("/api/hello-world"))
                .andExpect(status().isUnauthorized());

    }


    @Test
    void shouldReturnHelloWorld() throws Exception {
        MvcResult result = this.mvc.perform(post("/api/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(CREDENTIALS))
                .andExpect(status().isOk())
                .andReturn();

        String token = objectMapper.readValue(result.getResponse().getContentAsString(), Response.class).getToken();

        this.mvc.perform(get("/api/hello-world")
                .header("Authorization", "Bearer " + token))
                        .andExpect(content().string("{\"message\":\"Hello World\"}"));


    }

}