package com.todoapp.todoapp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.todoapp.todoapp.model.RegisterResponse;
import com.todoapp.todoapp.model.User;
import com.todoapp.todoapp.repository.UserRepository;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
class RegisterUserControllerTest {

    @Autowired
    private WebApplicationContext context;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    ObjectMapper objectMapper;

    private User user;

    private MockMvc mvc;

    @BeforeEach
    public void setup() {
        objectMapper.findAndRegisterModules();
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

    }

    @AfterEach
    public void clean() {
        userRepository.delete(user);
    }

    @Test
    void shouldCreateUser() throws Exception {
        MvcResult result = this.mvc.perform(post("/api/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("""
                                    {
                                        "email" : "test@g.com",
                                        "password" : "11111111",
                                        "confirmPass" : "11111111"
                                    }
                                """))
                .andExpect(status().isOk())
                .andReturn();
        RegisterResponse userResponse = objectMapper.readValue(result.getResponse().getContentAsString(), RegisterResponse.class);
        user = userRepository.findByEmail(userResponse.getRegisteredUser()).get();
        assertNotNull(user);

    }

    @Test
    void shouldReturnBadRequest() throws Exception {
        MvcResult result = this.mvc.perform(post("/api/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content("""
                                    {
                                        "email" : "kredek@g.com",
                                        "password" : "11111111",
                                        "confirmPass" : "11111111"
                                    }
                                """))
                .andReturn();
         this.mvc.perform(post("/api/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("""
                                    {
                                        "email" : "kredek@g.com",
                                        "password" : "11111111",
                                        "confirmPass" : "11111111"
                                    }
                                """))
                .andExpect(status().isBadRequest());

        RegisterResponse userResponse = objectMapper.readValue(result.getResponse().getContentAsString(), RegisterResponse.class);
        user = userRepository.findByEmail(userResponse.getRegisteredUser()).get();
    }

}