package com.todoapp.todoapp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.todoapp.todoapp.model.*;
import com.todoapp.todoapp.repository.TodoRepository;
import com.todoapp.todoapp.repository.UserRepository;
import org.junit.Before;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
class TodoControllerTest {

    @Autowired
    private WebApplicationContext context;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder encoder;
    @Autowired
    private TodoRepository todoRepository;

    @Autowired
    ObjectMapper objectMapper;
    private MvcResult resultWithToken;
    private MvcResult resultWithTodo;
    private MvcResult resultWithTodo2;

    private String token;

    private User user;

    private MockMvc mvc;

    private Todo todo;
    private TodoResponse todoResponse;


    @BeforeEach
    public void setup() throws Exception {
        objectMapper.findAndRegisterModules();
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

        user = userRepository.save(new User("kredek@g.com", encoder.encode("11111111"), "USER_ADMIN"));

        resultWithToken = this.mvc.perform(post("/api/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(CREDENTIALS))
                .andExpect(status().isOk())
                .andReturn();

        token = objectMapper.readValue(resultWithToken.getResponse().getContentAsString(), Response.class).getToken();

    }


    @AfterEach
    public void clean() {
        userRepository.delete(user);
    }

    private final String CREDENTIALS = """
                                          {
                                             "email": "kredek@g.com",
                                             "password": "11111111" 
                                          }
                                          """;

    @Test
    void shouldCreateAndReturnTodo() throws Exception {

        resultWithTodo = this.mvc.perform(post("/api/todo")
                        .header("Authorization", "Bearer " + token)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("""
                                    {
                                        "title" : "title",
                                        "status" : "done",
                                        "deadline" : "2019-01-21"
                                    }
                                """))
                .andExpect(status().isOk())
                .andReturn();

        String todoAsString = resultWithTodo.getResponse().getContentAsString();
        todoResponse = objectMapper.readValue(todoAsString, TodoResponse.class);

        assertNotNull(todoResponse);

    }

    @Test
    void shouldFindById() throws Exception {

        resultWithTodo = this.mvc.perform(post("/api/todo")
                        .header("Authorization", "Bearer " + token)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("""
                                    {
                                        "title" : "title",
                                        "status" : "done",
                                        "deadline" : "2019-01-21"
                                    }
                                """))
                .andExpect(status().isOk())
                .andReturn();

        String todoAsString = resultWithTodo.getResponse().getContentAsString();
        CreateTodoResponse todo = objectMapper.readValue(todoAsString, CreateTodoResponse.class);

        MvcResult mvcResult = this.mvc.perform(get("/api/todo/" + todo.getId())
                        .header("Authorization", "Bearer " + token))
                .andExpect(status().isOk())
                .andReturn();



        todoResponse = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), TodoResponse.class);

        assertNotNull(todo);


    }

    @Test
    void shouldFindAll() throws Exception {

        resultWithTodo = this.mvc.perform(post("/api/todo")
                        .header("Authorization", "Bearer " + token)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("""
                                    {
                                        "title" : "title",
                                        "status" : "done",
                                        "deadline" : "2019-01-21"
                                    }
                                """))
                .andExpect(status().isOk())
                .andReturn();

        resultWithTodo2 = this.mvc.perform(post("/api/todo")
                        .header("Authorization", "Bearer " + token)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("""
                                    {
                                        "title" : "title2",
                                        "status" : "todo",
                                        "deadline" : "2020-01-21"
                                    }
                                """))
                .andExpect(status().isOk())
                .andReturn();

        MvcResult mvcResult = this.mvc.perform(get("/api/todo")
                        .header("Authorization", "Bearer " + token))
                .andExpect(status().isOk())
                .andReturn();
        assertNotNull(mvcResult.getResponse().getContentType());

    }


    @Test
    void shouldUpdateById() throws Exception {

        resultWithTodo = this.mvc.perform(post("/api/todo")
                        .header("Authorization", "Bearer " + token)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("""
                                    {
                                        "title" : "title",
                                        "status" : "todo",
                                        "deadline" : "2019-01-21"
                                    }
                                """))
                .andExpect(status().isOk())
                .andReturn();


        TodoResponse originalTodo = objectMapper.readValue(resultWithTodo.getResponse().getContentAsString(), TodoResponse.class);

        this.mvc.perform(patch("/api/todo/" + originalTodo.getId())
                        .header("Authorization", "Bearer " + token)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("""
                                    {
                                        "title" : "updatedTitle",
                                        "status" : "done",
                                        "deadline" : "2020-01-21"
                                    }
                                """))
                .andExpect(status().isOk());

        Todo updatedTodo = todoRepository.findById(originalTodo.getId()).get();

        assertEquals("updatedTitle", updatedTodo.getTitle());
        assertEquals("done", updatedTodo.getStatus());

    }

    @Test
    void shouldDeleteById() throws Exception {
        resultWithTodo = this.mvc.perform(post("/api/todo")
                        .header("Authorization", "Bearer " + token)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("""
                                    {
                                        "title" : "title",
                                        "status" : "done",
                                        "deadline" : "2019-01-21"
                                    }
                                """))
                .andExpect(status().isOk())
                .andReturn();

        String todoAsString = resultWithTodo.getResponse().getContentAsString();
        todoResponse = objectMapper.readValue(todoAsString, TodoResponse.class);

        MvcResult mvcResult = this.mvc.perform(delete("/api/todo/" + todoResponse.getId())
                        .header("Authorization", "Bearer " + token))
                .andExpect(status().isNoContent())
                .andReturn();

        Optional<Todo> todoOptional = todoRepository.findById(todoResponse.getId());
        assertTrue(todoOptional.isEmpty());

    }
}